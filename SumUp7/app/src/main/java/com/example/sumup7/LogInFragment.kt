package com.example.sumup7

import android.text.Editable
import android.util.Log
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.sumup7.base.BaseFragment
import com.example.sumup7.databinding.FragmentLogInBinding
import com.example.sumup7.model.LoginRequest
import com.example.sumup7.model.UserProfileViewModel
import com.example.sumup7.network.RetrofitInstance
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.security.auth.callback.Callback

class LogInFragment : BaseFragment<FragmentLogInBinding, UserProfileViewModel>(FragmentLogInBinding::inflate) {
    override fun getViewModelClass() = UserProfileViewModel::class.java
    override var useSharedViewModel = true

    var dataStoreManager = context?.let { DataStoreManager(it) }
    private lateinit var token:String
    override fun start() {
        token = ""
        listeners()
        resListeners()

    }

    private fun listeners() {
        binding.register.setOnClickListener {
            findNavController().navigate(R.id.action_logInFragment_to_registerFragment)
        }

        binding.loginBtn.setOnClickListener {
            makeGetRequest()
          /*  val result = binding.email.text.toString()
            setFragmentResult("requestKey3", bundleOf("bundleKey" to result))
            findNavController().navigate(R.id.action_logInFragment_to_homeFragment)*/
        }


    }

    private fun remember() {

       val isCHekd = binding.switchRemember.isChecked
        if(isCHekd) {

        }

    }


    private fun resListeners() {
        setFragmentResultListener("requestKey") { requestKey, bundle ->
            Log.i("requestKey", "some request key")
            val result = bundle.getString("bundleKey")
            if(result != null) {
                binding.email.hint = result.toString()
            }

        }

        setFragmentResultListener("requestKey2") { requestKey, bundle ->
            Log.i("requestKeyP", "pass request key")
            val result2 = bundle.getString("bundleKey2")
            if(result2 != null) {
                binding.password.hint = result2.toString()
            }

        }
    }

    private suspend fun handleToken() {
        Log.i("handlTOk", "aq modis")
        dataStoreManager?.getFromDataStore()?.observe(viewLifecycleOwner, {
            if(it.isNotEmpty()) {
                viewModel.getLoginInfo(it)
                Log.i("token0", "token exist")
                viewModel.loginInfo.observe(viewLifecycleOwner, { respons ->
                    if(respons.items != null) {
                        Log.i("token1", "token exist")
                        val result = binding.email.text.toString()
                        setFragmentResult("requestKey3", bundleOf("bundleKey" to result))
                        findNavController().navigate(R.id.action_logInFragment_to_homeFragment)
                        //binding.email.hint = it.items.email.toString()
                        //binding.password.hint = it.items.password.toString()
                    }
                })
                // binding.email.text = it.email as Editable
                // binding.password.text = it.password as Editable
            } else {
                Log.i("token2", "token does not exist")
                Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun makeGetRequest() {
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(IO) {
                Log.i("getReq" , "makes get request")
                handleToken()
            }
        }

    /*    viewModel.getLoginInfo(token)

        viewModel.loginInfo.observe(viewLifecycleOwner, {
            if(it.items != null) {

                val result = binding.email.text.toString()
                setFragmentResult("requestKey3", bundleOf("bundleKey" to result))
                findNavController().navigate(R.id.action_logInFragment_to_homeFragment)
                //binding.email.hint = it.items.email.toString()
                //binding.password.hint = it.items.password.toString()
            }
        })*/
    }
}