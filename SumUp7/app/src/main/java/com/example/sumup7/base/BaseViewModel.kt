package com.example.sumup7.base

import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.sumup7.Repository


abstract class BaseViewModel(private val repository: Repository): ViewModel()