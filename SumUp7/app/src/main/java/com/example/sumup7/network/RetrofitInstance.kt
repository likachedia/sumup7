package com.example.sumup7.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitInstance {
    val BASE_URL = "https://reqres.in/api/"

    private val client = OkHttpClient.Builder().apply {
        addInterceptor(Userinterceptor())
    }.build()
//            .client(client)
    private val retrofit by lazy{
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(moshiConvertor())
            .build()
    }

    val api: SimpleApi by lazy {
        retrofit.create(SimpleApi::class.java)
    }

    private fun moshiConvertor() =
        MoshiConverterFactory.create(
            Moshi.Builder().addLast(KotlinJsonAdapterFactory())
                .build()
        )
}