package com.example.sumup7.model

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope

import com.example.sumup7.base.BaseViewModel
import com.example.sumup7.LoginResponse
import com.example.sumup7.Repository
import com.example.sumup7.Resource
import com.example.sumup7.network.RetrofitInstance
import kotlinx.coroutines.launch
import retrofit2.Response


class UserProfileViewModel(private val repository:Repository): BaseViewModel(repository) {

   // val dataStoreManager = DataStoreManager(context)
   // val protoManeger = ProtoManager(context)
    val myResponse: MutableLiveData<UiState> = MutableLiveData()

    private val _loginInfo:MutableLiveData<ResponseState> = MutableLiveData()
    val loginInfo:LiveData<ResponseState> get() = _loginInfo

    suspend fun pushPOst(post: LoginRequest): Response<LoginResponse> {
        return RetrofitInstance.api.pushPost(post)
    }
    init {
        //fetchData()
    }

   fun post(post: LoginRequest) {
       viewModelScope.launch {
           val respons = pushPOst(post)
           //myResponse.value = respons
       }

    }

    fun fetchData(post:LoginRequest) {

        viewModelScope.launch {
            val data = repository.pushPost(post)
            Log.i("data", data.toString());
            when(data) {
                is Resource.Success -> {
                    myResponse.value = UiState(items = (data.data))
                   // Log.i("uistate-1", _uiState.toString())
                }
                is Resource.Error -> {
                    myResponse.value = UiState(error = UiState.Error.ClientError)
                }
            }
        }
    }

    fun getLoginInfo(token:String) {
        viewModelScope.launch {
            val data = repository.getPost(token)
            Log.i("data1", data.toString());
            when(data) {
                is Resource.Success -> {
                    _loginInfo.value = ResponseState(items = data.data)
                    // Log.i("uistate-1", _uiState.toString())
                }
                is Resource.Error -> {
                    _loginInfo.value = ResponseState(error = ResponseState.Error.ClientError)
                }
            }
        }
    }

    data class UiState(
        val isLoading: Boolean = false,
        val error:Error? = null,
        val items: LoginResponse? = null
    ) {
        sealed class Error {
            object ClientError: Error()
        }
    }

    data class ResponseState(
        val isLoading: Boolean = false,
        val error:Error? = null,
        val items: LoginRequest? = null
    ) {
        sealed class Error {
            object ClientError: Error()
        }
    }


}