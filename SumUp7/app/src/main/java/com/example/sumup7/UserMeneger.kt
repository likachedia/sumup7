package com.example.sumup7

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.flow.map

val USER_DATASTORE = "userInfo"
val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = USER_DATASTORE)
class DataStoreManager(val context: Context) {


    companion object {

        val TOKEN = stringPreferencesKey("NAME")
        val EMAIL = stringPreferencesKey("email")
          val  PASSWORD = stringPreferencesKey("password")
    }



    suspend fun savetoDataStore(token:String) {
        context.dataStore.edit {
            it[TOKEN] = token
        }
    }

    suspend fun getFromDataStore() = context.dataStore.data.map {
            it[TOKEN]?:""


    }.asLiveData()

    suspend fun getEmial() = context.dataStore.data.map {
        it[EMAIL]?:""
    }.asLiveData()

    suspend fun getPassword() = context.dataStore.data.map {
        it[PASSWORD]?:""
    }.asLiveData()

    data class Userinfo(
        val token:String,
        val email:String,
        val password: String
    ) {}
}