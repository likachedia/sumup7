package com.example.sumup7

import android.text.Editable
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.example.sumup7.base.BaseFragment
import com.example.sumup7.databinding.FragmentHomeBinding
import com.example.sumup7.model.UserProfileViewModel

class HomeFragment : BaseFragment<FragmentHomeBinding, UserProfileViewModel>(FragmentHomeBinding::inflate) {
    override fun getViewModelClass() = UserProfileViewModel::class.java

    override fun start() {
        getData()
        listeners()
    }

    private fun getData() {
        setFragmentResultListener("requestKey3") { requestKey, bundle ->

            val result = bundle.getString("bundleKey3")
            binding.registeredEmail.text = result as Editable
        }
    }

    private fun listeners() {
        binding.logOut.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_logInFragment)
        }
    }

}