package com.example.sumup7

import com.google.gson.annotations.SerializedName



    data class User (

        var id: String,
        var firstName: String,
        var lastName: String,
        var email: String
    )
