package com.example.sumup7

import android.util.Log
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.sumup7.base.BaseFragment
import com.example.sumup7.databinding.FragmentRegisterBinding
import com.example.sumup7.model.LoginRequest
import com.example.sumup7.model.UserProfileViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegisterFragment : BaseFragment<FragmentRegisterBinding, UserProfileViewModel>(FragmentRegisterBinding::inflate) {
    override var useSharedViewModel = true
        set(value) {}
    override fun getViewModelClass() = UserProfileViewModel::class.java
    var dataStoreManager = context?.let { DataStoreManager(it) }
    private lateinit var token:String
    override fun start() {
        token = ""
        listeners()
    }

    private fun listeners() {
        binding.register.setOnClickListener {
            observer()
            //findNavController().navigate(R.id.action_registerFragment_to_logInFragment)
        }
        binding.Back.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_logInFragment)
        }
    }

    private suspend fun addTokenToDataStore(token:String) {
        dataStoreManager?.savetoDataStore(token)
    }
    private fun observer() {
        val email =  binding.email.text.toString()
        val password=  binding.password.text.toString()
      /*  viewLifecycleOwner.lifecycleScope.launch {
            withContext(Dispatchers.Main){
                handleToken()
            }
        }*/

        viewLifecycleOwner.lifecycleScope.launch {
            withContext(Main) {
                handleToken()
                sendDataToLogInFr()
            }

            val model = LoginRequest(email, password, token)
            viewModel.fetchData(model)

            viewModel.myResponse.observe(viewLifecycleOwner, {
                if(it.items?.authToken!!.isNotEmpty()) {
                    viewLifecycleOwner.lifecycleScope.launch {
                        withContext(Dispatchers.IO) {
                            Log.i("token", it.items.authToken.toString())
                            addTokenToDataStore(it.items.authToken.toString())
                            sendDataToLogInFr()

                        }
                    }

                }
            })
        }
    }

    private fun sendDataToLogInFr() {
        val result = binding.email.text.toString()
        val result2 = binding.password.text.toString()
        // Use the Kotlin extension in the fragment-ktx artifact
        Log.i("requestKeyR", "it;s from register fragment")
        setFragmentResult("requestKey", bundleOf("bundleKey" to result))
        setFragmentResult("requestKey2", bundleOf("bundleKey2" to result2))
        findNavController().navigate(R.id.action_registerFragment_to_logInFragment)
    }

    private suspend fun handleToken() {
        Log.i("handl", "it;s from register fragment")
        dataStoreManager?.getFromDataStore()?.observe(viewLifecycleOwner, {
            if(it.isNotEmpty()) {
                token = it
                // binding.email.text = it.email as Editable
                // binding.password.text = it.password as Editable
            }
        })
    }
}