package com.example.sumup7

import android.util.Log
import androidx.paging.PagingSource
import com.example.sumup7.model.LoginRequest
import com.example.sumup7.network.RetrofitInstance
import okio.IOException
import retrofit2.HttpException
import retrofit2.Response


class Repository {

    suspend fun pushPost(post:LoginRequest): Resource<LoginResponse> {
        return try {
            val response = RetrofitInstance.api.pushPost(post)
            Log.i("response: ", response.toString())
            val result = response.body()
            if(response.isSuccessful && result != null) {
                Log.i("response: ", response.toString())
               Resource.Success(result)

            } else {
                Log.i("response: ", response.message().toString())
                Resource.Error("Something went wrong")
            }

        }catch (exception: IOException) {
            Resource.Error(exception.toString())
        } catch (exception: HttpException) {
            Resource.Error(exception.toString())
        }

    }

    suspend fun getPost(tokenFromRe:String): Resource<LoginRequest>{
        return try {
            val response = RetrofitInstance.api.getCustomPost(tokenFromRe)
            Log.i("response: ", response.toString())
            val result = response.body()
            if(response.isSuccessful && result != null) {
                Log.i("response: ", response.toString())
                Resource.Success(result)

            } else {
                Log.i("response: ", response.message().toString())
                Resource.Error("Something went wrong")
            }

        }catch (exception: IOException) {
            Resource.Error(exception.toString())
        } catch (exception: HttpException) {
            Resource.Error(exception.toString())
        }
    }

}