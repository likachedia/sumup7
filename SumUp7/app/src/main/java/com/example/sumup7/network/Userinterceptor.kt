package com.example.sumup7.network

import android.content.Context
import com.example.sumup7.DataStoreManager
import com.example.sumup7.LoginResponse
import okhttp3.Interceptor

import okhttp3.Response

class Userinterceptor:Interceptor{
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .addHeader("Auth-Token", "QpwL5tke4Pnpja7X4")
            .build()
        return chain.proceed(request)
    }
}