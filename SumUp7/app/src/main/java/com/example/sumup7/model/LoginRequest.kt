package com.example.sumup7.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginRequest(
    val email:String?,
    val password:String?,
    var authToken: String?,
) {
}