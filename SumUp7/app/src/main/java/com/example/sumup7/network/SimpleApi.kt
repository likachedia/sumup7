package com.example.sumup7.network


import com.example.sumup7.LoginResponse
import com.example.sumup7.model.LoginRequest
import retrofit2.Response
import retrofit2.http.*
import retrofit2.http.GET




interface SimpleApi {

  @GET("login")
    suspend fun getCustomPost(
      @Header("Content-Range") contentRange: String?
    ): Response<LoginRequest>


  @POST("login")
   suspend fun pushPost(
     @Body request: LoginRequest
   ): Response<LoginResponse>

}