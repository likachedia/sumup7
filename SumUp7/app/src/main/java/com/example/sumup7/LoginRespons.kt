package com.example.sumup7

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class LoginResponse (
    @Json(name = "token")
    var authToken: String?,
    var id: Int?
)